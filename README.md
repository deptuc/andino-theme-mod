# portal-andino-theme

Repositorio de la extensión del Portal Andino de la República Argentina para [CKAN](http://ckan.org/). Este proyecto se encarga de modificaciones al ruteo de la aplicación web, cambios visuales a la interfaz, customización del portal, analytics y gestión de usuarios, permisos y roles, entre otros. Este repositorio *no* constituye el proyecto entero. El repositorio central del proyecto del Portal Andino es [portal-andino](https://github.com/datosgobar/portal-andino)

- [Instalación](#instalaci%C3%B3n)
- [Desarrollo](#desarrollo)
- [Uso del theme](#uso-del-theme)
- [Créditos](#cr%C3%A9ditos)
- [Consultas sobre Andino](#consultas-sobre-andino)
- [Contacto](#contacto)

## Instalación

instrucciones de instalacion:
[Fuente](https://portal-andino.readthedocs.io/es/stable/developers/install/)
## Instalación avanzada de andino

La instalación avanzada está pensada para usuarios que quieren ver cómo funciona internamente Andino

Para instalar y ejecutar Andino, seguiremos estos pasos:

### Paso 1: Clonar repositorio.

sudo mkdir /etc/portal
cd /etc/portal
sudo git clone https://github.com/datosgobar/portal-andino.git .

### Paso 2: Especificar las variables de entorno para el contenedor de postgresql.

NOTA: Debemos usar un dominio válido para la variable DOMINIO, de otra forma el envio de mails no funcionará. Postfix require un "fully-qualified domain name (FQDN)". Ver la documentación de Postfix para más detalles.

DB_USER=<my user>
DB_PASS=<my pass>
DOMINIO=andino.midominio.com.ar
ANDINO_VERSION=<version que deseamos instalar>
sudo su -c "echo POSTGRES_USER=$DB_USER > .env"
sudo su -c "echo POSTGRES_PASSWORD=$DB_PASS >> .env"
sudo su -c "echo NGINX_HOST_PORT=80 >> .env"
sudo su -c "echo DATASTORE_HOST_PORT=8800 >> .env"
sudo su -c "echo maildomain=$DOMINIO >> .env"
sudo su -c "echo ANDINO_TAG=$ANDINO_VERSION >> .env"

Nota: También definir puerto https o borrar la linea del archivo latest.yml donde mapea al puerto 443

### Paso 3: Construir y lanzar los contenedor de servicios usando el archivo latest.yml

    sudo docker-compose -f latest.yml up -d db postfix redis solr

### Paso 4: Construir y lanzar el contenedor de andino usando el archivo latest.yml

    sudo docker-compose -f latest.yml up -d portal

### Paso 5: Inicializar la base de datos y la configuración de la aplicación:

EMAIL=admin@example.com
HOST=datos.gob.ar
DB_USER=<my db user>
DB_PASS=<my db pass>
STORE_USER=<my datastore user>
STORE_PASS=<my datastore password>
docker-compose -f latest.yml exec portal /etc/ckan_init.d/init.sh -e "$EMAIL" -h "$HOST" \
        -p "$DB_USER" -P "$DB_PASS" \
        -d "$STORE_USER" -D "$STORE_PASS"

### Paso 6: Construir el contenedor de nginx usando el archivo latest.yml

    $ sudo docker-compose -f latest.yml up -d nginx

## Desinstalar andino

Esta secuencia de comandos va a ELIMINAR TODOS LOS CONTENEDORES, IMÁGENES y VOLUMENES de la aplicación de la vm donde está instalada la plataforma.

Esta operación no es reversible. Perderás todos tus datos si realizas esta operación.

app_dir="/etc/portal/"
cd $app_dir
docker-compose -f latest.yml down -v
cd ~/
sudo rm $app_dir -r


## Administración de usuarios
### Crear un usuario ADMIN

docker-compose -f latest.yml exec portal /etc/ckan_init.d/add_admin.sh mi_nuevo_usuario_admin email_del_usuario_admin

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.

El comando solicitará la contraseña del usuario administrador.
### Listar mis usuarios

docker-compose -f latest.yml exec portal /etc/ckan_init.d/paster.sh --plugin=ckan user list

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.
### Ver los datos de un usuario

docker-compose -f latest.yml exec portal /etc/ckan_init.d/paster.sh --plugin=ckan user nombre-de-usuario

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.
### Crear un nuevo usuario

docker-compose -f latest.yml exec portal /etc/ckan_init.d/paster.sh --plugin=ckan user add nombre-de-usuario

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.
### Crear un nuevo usuario extendido

docker-compose -f latest.yml exec portal /etc/ckan_init.d/paster.sh --plugin=ckan user add nomber [email=mi-usuario@host.com password=mi-contraseña-rara apikey=unsecretomisticonoleible]

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.
### Eliminar un usuario

docker-compose -f latest.yml exec portal /etc/ckan_init.d/paster.sh --plugin=ckan user remove nombre-de-usuario

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.
### Cambiar password de un usuario

docker-compose -f latest.yml exec portal /etc/ckan_init.d/paster.sh --plugin=ckan user setpass nombre-de-usuario

Ver la sección sobre la utilización del archivo latest.yml en los comandos de docker-compose.






## mantenimiento:
https://portal-andino.readthedocs.io/es/stable/developers/maintenance/

    +-----------------------------------+
    | datos de la instalación de prueba |-------------+
    +-----------------------------------+             |
    | export EMAIL=admin@example.com                  |
    | export HOST=localhost                           |
    | export DB_USER=root                             |
    | export DB_PASS=root                             |
    | export STORE_USER=web                           |
    | export STORE_PASS=dep1234                       |
    +-------------------------------------------------+


    +----------------------------+
    | archivos de tema de andino |--------------------------------------------+
    +----------------------------+                                            |
    | docker: datosgobar/portal-andino:latest "/etc/ckan_init.d/st…" andino   |
    | /usr/lib/ckan/default/src/ckanext-gobar-theme/ckanext/gobar_theme/      |
    +-------------------------------------------------------------------------+
    |  En esta carpeta estan los archivos que diferencian (mayormente) a      |
    | andino de ckan.                                                         |
    |  Se encuentran archivos python que renderizan vistas definidas por      |
    | templates Jinja (http://jinja.pocoo.org/docs/2.10/templates/).          |
    | La base de datos de postgresql (docker andino-db) guarda en la tabla    |
    | groups tanto temas como organizaciones, diferenciandolos por un campo   |
    | type (organization o group).                                            |
    | Por lo tanto para agregar el filtrado por ODS, que funcione igual que   |
    | los temas, se tendrá que duplicar los controladores referentes a temas, |
    | agregando otra variacion al campo type (ods).                           |
    +-------------------------------------------------------------+-----------+
    | scp -r /usr/lib/ckan/default/src/ckanext-gobar-theme/ \     |
    | usuario7@172.21.0.1:/home/usuario7/andino-theme             |
    | scp -r usuario7@172.21.0.1:/home/usuario7/andino-theme/* \  |
    | /usr/lib/ckan/default/src/ckanext-gobar-theme/              |
    +-------------------------------------------------------------+


    +--------------------------------------------+
    | modificaciones de codigo realizadas por mi |----------------------------------+
    +--------------------------------------------+                                  |
    | * agregar en el menu superior el enlace a la vista de ods (en proceso)        |
    | * separar temas con "ods-" incluido en su nombre ("ODS ") de los otros temas, |
    |    usando los template jinja                                                  |
    +-------------------------------------------------------------------------------+


    +-----------------------------------------+
    | correciones de codigo realizadas por mi |------------------------------------+
    +-----------------------------------------+                                    |
    | en el archivo (ruta respecto a la carpeta de ckanext-gobar-theme)            |
    | .../js/package/resource/urls_and_icons.js                                    |
    | linea 15: "$(document).on('click', '.file-upload-label', function (event) {" |
    | event tiene que ser parámetro de entrada de la funcion, sino el botón de     |
    | subir no funciona.                                                           |
    +------------------------------------------------------------------------------+

La instalación del paquete completo original está disponible como un contenedor de Docker. Seguir las instrucciones del repositorio del [Portal Andino](https://github.com/datosgobar/portal-andino) para levantar la instancia con Docker.


## Desarrollo

Como alternativa a la instalación dockerizada existe la posibilidad de tener una instalación contenida en un `virtualenv` del sistema. Esto se puede obtener siguiendo las instrucciones de [esta guia](http://docs.ckan.org/en/ckan-2.5.2/maintaining/installing/install-from-source.html). Una vez instalado el paquete a nivel sistema, es posible linkear el proceso principal a un debbuger de python (por ej pycharm). Este metodo no es recomendado para hacer modificaciones que impacten en el manejo del servidor por parte del wsgi de apache o nginx. Para dicho caso, es necesario tener una instalación de la aplicación dockerizada y acceder al contenedor del theme para realizar el desarrollo necesario.

Esta extensión de ckan fue desarrollada siguiendo la [guia de creación de extensiones](http://docs.ckan.org/en/ckan-2.5.2/extensions/tutorial.html).

### Estructura de archivos

```
- ckanext
    - gobar_theme
        - js
            - archivos de js a ser importados por los distintos templates html
        - public
            - assets estáticos y públicos como imagenes y fuentes
        - styles
            - archivos css generados desde sus versiones de scss
        - templates
            - archivos de jinja renderizados por los controladores
        - actions.py # lógica de modelos de ckan, sobreescribe y/o extiende la lógica de ckan
        - config_controller.py # controlador de lógica para customización del portal
        - controller.py # controladores para la home y la api, sobreescriben y/o extienden la lógica de ckan
        - google_analytics_controller.py # controlador de google analytics, sobreescribe y/o extiende la lógica de la extensión de analytics
        - helpers.py # metodos auxiliares para renderizado de templates
        - mailer.py # metodos relacionados al envio de mails
        - package_controller.py # controlador de lógica de datasets y recursos, sobreescribe y/o extiende la lógica de ckan
        - plugin.py # archivo que registra el repositorio como extensión de ckan y declara acciones, helpers y ruteo
        - routing.py # asociación de rutas a controladores y redireccionamientos, sobreescribe y/o extiende las de ckan
        - user_controller.py # controlador de lógica de usuarios, sobreescribe y/o extiende la lógica de ckan
```

## Uso del theme

Está disponible una [guía de uso](./docs/guia_uso_portal_andino.md) dentro de la documentación de este repositorio.

## Créditos

Este repositorio es un fork de la extensión de CKAN de [datos.gob.ar](https://github.com/datosgobar/datos.gob.ar)

## Consultas sobre Andino

**Andino es un portal abierto en constante desarrollo** para ser usado por toda la comunidad de datos. Por eso, cuando incorporamos una nueva mejora, **cuidamos mucho su compatibilidad con la versión anterior**.

Como la comunidad de datos es grande, **por ahora no podemos dar soporte técnico frente a modificaciones particulares del código**. Sin embargo, **podés contactarnos para despejar dudas**.

## Contacto

Te invitamos a [crearnos un issue](https://github.com/datosgobar/portal-andino-theme/issues/new?title=Encontre%20un%20bug%20en%20andino) en caso de que encuentres algún bug o tengas feedback de alguna parte de `portal-andino-theme`.

Para todo lo demás, podés mandarnos tu comentario o consulta a [datos@modernizacion.gob.ar](mailto:datos@modernizacion.gob.ar).
